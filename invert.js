function invert(obj){

    const newobj = {};
    for(let key in obj){
        newobj[obj[key]] = key;
    }
    return newobj;

}
invert({
    Tony:57,
    Steve:95,
    Bruce:45,
    TChalla:39,
    Natasha:35
})